The challenge of this year is about dermoscopic image classification. It a 8-class classification problem and it will be evaluated in this Kaggle website: https://www.kaggle.com/c/ima205-challenge-2022/overview

The challenge is restricted, which means that in order to participate to the challenge you must use the following link to have access:

https://www.kaggle.com/t/4c243c2af21a40c4884022da9a3b7929

You can use either your name and surname or a nickname (please write the nickname in the [report](https://ecampus.paris-saclay.fr/mod/assign/view.php?id=461048))

Please look at the pdf attached if you want to use the computational resources at Télécom. A copy of the Challenge data can be found here: 

/tsi/data_education/ChallengeIMA205
