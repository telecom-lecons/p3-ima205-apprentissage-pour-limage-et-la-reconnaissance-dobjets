import os
import cv2
import skimage
import sklearn
import scipy
import matplotlib
import PIL
#import sys
#import argparse

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import pandas_profiling as pdp
import matplotlib.pyplot as plt
import seaborn as sns
#import xgboost as xgb

from skimage.measure import label, regionprops, regionprops_table
from skimage import morphology, measure, segmentation, exposure, color
from skimage.filters import sobel
from scipy.ndimage import uniform_filter
from tqdm import tqdm
from os import listdir
from os.path import isfile, join
from cv2 import imread
from PIL import Image
from sklearn import preprocessing
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import accuracy_score

from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, BatchNormalization
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Input
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.optimizers import Adam

fold = '/home/gbittencourt/Documents/REPOS/TELECOM/P3/p3-ima205-apprentissage-pour-limage-et-la-reconnaissance-dobjets/00_Challenge/dataset/'
train_path = fold+'Train/Train/'
test_path = fold+'Test/Test/'

train_df = pd.read_csv(fold+'metadataTrain.csv')
test_df = pd.read_csv(fold+'metadataTest.csv')

values = {"SEX": 'male', "AGE":train_df.AGE.mean(), "POSITION" : 'anterior torso'}

train_df.fillna(value=values, inplace = True)
test_df.dropna()

position_mapping = {
    'anterior torso'  : 1,
    'lower extremity' : 2,
    'head/neck'       : 3,
    'upper extremity' : 4,
    'posterior torso' : 5,
    'palms/soles'     : 6,
    'oral/genital'    : 7,
    'lateral torso'   : 8
}

sex_mapping = {
    'male'   : 0,
    'female' : 1
}

classes_dictionnary = { 1 : 'Melanoma',
2 : 'Melanocytic nevus',
3 : 'Basal cell carcinoma',
4 : 'Actinic keratosis',
5 : 'Benign keratosis',
6 : 'Dermatofibroma',
7 : 'Vascular lesion',
8 : 'Squamous cell carcinoma'}

for pos in position_mapping:
    train_df.loc[train_df['POSITION'] == pos,'POSITION'] = position_mapping[pos]
    test_df.loc[test_df['POSITION'] == pos,'POSITION'] = position_mapping[pos]
for sex in sex_mapping:
    train_df.loc[train_df['SEX'] == sex,'SEX'] = sex_mapping[sex]
    test_df.loc[test_df['SEX'] == sex,'SEX'] = sex_mapping[sex]

train_df

def img_Y(img):
    return (0.299 * img[:,:,0] + 0.587 * img[:,:,1] + 0.114 * img[:,:,2])

def hair_removal(img, lowbound=5, filter_size=7, inpaintmat=5):
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    #applying a blackhat
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (filter_size, filter_size)) 
    img_morp = cv2.morphologyEx(img_gray, cv2.MORPH_BLACKHAT, kernel)

    #0=skin and 255=hair
    ret, mask = cv2.threshold(img_morp, lowbound, 255, cv2.THRESH_BINARY)
    
    #inpainting
    img_final = cv2.inpaint(img, mask, inpaintmat ,cv2.INPAINT_TELEA)

    return img_final

# https://github.com/AndersDHenriksen/SanityChecker/blob/master/AllChecks.py
def bwareafilt(mask, n=1, area_range=(0, np.inf)):
    """Extract the largest white area of binary img"""
    # For openCV > 3.0 this can be changed to: areas_num, labels = cv2.connectedComponents(mask.astype(np.uint8))
    areas_num, labels = cv2.connectedComponents(mask.astype(np.uint8))
    labels = measure.label(mask.astype('uint8'), background=0)
    area_idx = np.arange(1, np.max(labels) + 1)
    areas = np.array([np.sum(labels == i) for i in area_idx])
    inside_range_idx = np.logical_and(areas >= area_range[0], areas <= area_range[1])
    area_idx = area_idx[inside_range_idx]
    areas = areas[inside_range_idx]
    keep_idx = area_idx[np.argsort(areas)[::-1][0:n]]
    kept_areas = areas[np.argsort(areas)[::-1][0:n]]
    
    if np.size(kept_areas) == 0:
        kept_areas = np.array([0])
    if n == 1:
        kept_areas = kept_areas[0]
    kept_mask = np.isin(labels, keep_idx)
    
    return kept_mask

def image_resizing(img):
    h, w = img.shape[0], img.shape[1]
    if h < w:
        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        h, w = img.shape[0], img.shape[1]
    new_w = 500
    new_h = int(new_w*h / w)
    img = cv2.resize(img,(new_w,new_h))

    x = int(new_w/2 - 250)
    y = int(new_h/2 - 250)
    new_h = 500
    return img[y:y+new_h, x:x+new_w, :]

def segment_image(img):
    # remove hair and resize images
    img = image_resizing(img)
    img_filtered = hair_removal(img)

    # denoise luminance
    imgY = img_Y(img_filtered)
    img_invY = imgY.mean() - imgY; img_invY[img_invY < 0] = 0
    luminance = np.uint8(img_invY)
    luminance = cv2.fastNlMeansDenoising(luminance)

    # average image
    kerSize = 20; ker = np.ones((kerSize,kerSize))/kerSize**2
    avg_luminance = cv2.filter2D(luminance, -1, ker)

    # bwareafilt
    binaryImage = bwareafilt(avg_luminance > avg_luminance.std() / 2, area_range = (0, avg_luminance.shape[0]*avg_luminance.shape[1]/2))
    
    # remove borders
    if (binaryImage[0:15,:]).all() and (binaryImage[:,0:15]).all() and (binaryImage[-16:-1,:]).all() and (binaryImage[:,-16:-1]).all():
        i=15; j=15
        while((binaryImage[i,:]).all()):
            i+=1
        while((binaryImage[:,j]).all()):
            j+=1

        img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        img_gray = img_gray[i:img_gray.shape[0]-i,j:img_gray.shape[1]-j]
        edge = sobel(img_gray)

        kerSize = 20; ker = np.ones((kerSize,kerSize))/kerSize**2
        avg_img_gray = cv2.filter2D(edge, -1, ker)

        kerSize = 10; ker = np.ones((kerSize,kerSize))/kerSize**2
        img_processed = img_gray.mean() - abs(avg_img_gray - img_gray.mean())
        avg_img_process = cv2.filter2D(img_processed, -1, ker)
        avg_img_process = 1 - avg_img_process
        
        binaryImage = bwareafilt(avg_img_process > avg_img_process.mean(), area_range = (0, binaryImage.shape[0]*binaryImage.shape[1]/2))
    
    img_seg = cv2.bitwise_not(binaryImage*255)
    contour,hier = cv2.findContours(binaryImage*255,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contour:
        cv2.drawContours(img_seg,[cnt],0,255,-1)
    img_seg = uniform_filter(img_seg, size = 3)
    return img_seg

def get_features(df,f_path):
    files = [f for f in listdir(f_path) if isfile(join(f_path, f))]
    for id in tqdm(df.ID):
        if (id + '_seg.png') in files:
            seg = True
        else:
            seg = False
        img_rgb = cv2.cvtColor(cv2.imread(f_path+id+'.jpg'), cv2.COLOR_BGR2RGB)
        if seg:
            img_masked = image_resizing(cv2.imread(f_path+id+'_seg.png'))[:,:,0]
        else:
            img_masked = segment_image(img_rgb)

        props = regionprops_table(label(img_masked), properties=('centroid',
                                                                 'area',
                                                                 'axis_major_length',
                                                                 'axis_minor_length',
                                                                 'eccentricity',
                                                                 'equivalent_diameter_area',
                                                                 'perimeter'))
        df.loc[df['ID'] == id,'AREA'] = props['area'][0]
        df.loc[df['ID'] == id,'AXIS_MAJOR'] = props['axis_major_length'][0]
        df.loc[df['ID'] == id,'AXIS_MINOR'] = props['axis_minor_length'][0]
        df.loc[df['ID'] == id,'ECCENTRICITY'] = props['axis_minor_length'][0]
        df.loc[df['ID'] == id,'PERIMETER'] = props['perimeter'][0]

get_features(train_df,train_path)
train_df.to_csv(fold+'metadataCompleteTrain.csv')
get_features(test_df,test_path)
test_df.to_csv(fold+'metadataCompleteTest.csv')
